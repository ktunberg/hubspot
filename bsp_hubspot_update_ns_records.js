/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 Aug 2017     Karl Tunberg
 *
 */
if (typeof (BSP) == 'undefined') {
	BSP = {
		__namespace : true
	};
}
/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */

BSP.HSP_SCH = {
	updateNSCompanyRecs : function(type){
		this.updateNSRecs(1, 'companyId');
	},
	updateNSContactRecs : function(type){
		this.updateNSRecs(1, 'companyId');
		this.updateNSRecs(2, 'vid');
	},
	updateNSRecs : function(recType, idProp){
		try {
			var qResults = BSP.HSP_UTILS.getQRecs(recType);
			if (!qResults)return;
			var mapRecResults = BSP.HSP_UTILS.getMapFields(recType, true, false);
			if (!mapRecResults)return;
			var mapFields = [];
			for (var j = 0; j < mapRecResults.length; j++){
				mapFields.push({
					netsuiteId	:mapRecResults[j].getValue('custrecord_bsp_hsp_netsuite_fld_id'),
					hubspotId	:mapRecResults[j].getValue('custrecord_bsp_hsp_property'),
					subrecord	:mapRecResults[j].getValue('custrecord_subrecord')
				});
			}
			for (var i = 0; i < qResults.length; i++){
				try{
					var qData = null;
					var qDataString = qResults[i].getValue('custrecord_bsp_hsp_hubspot_data');
					if (qDataString)qData = JSON.parse(qDataString);
					var hubspotId = qData[idProp].toString();
					var recId = null;
					if (recType == 2) {
						recId = BSP.HSP_UTILS.findContactByHubspotId(hubspotId);
					}
					else{
						recId = BSP.HSP_UTILS.findCompanyByHubspotId(hubspotId);
					}
					var recordType = 'customer';
					if (recordType == 2)recordType = 'contact';
					var sysNotes = BSP.HSP_UTILS.getSystemNotesAfter(recordType,recId);
					qData = BSP.HSP_UTILS.removeOutdatedPropertiesInbound(qData, recType, hubspotId, recId);
					if (recType == 2){
						BSP.HSP_UTILS.updateNetsuiteContact(recId, hubspotId, mapFields, qData);
					}
					else{
						BSP.HSP_UTILS.updateNetsuiteCompany(recId, hubspotId, mapFields, qData);
					}
					nlapiSubmitField('customrecord_bsp_hubspot_inbound_q', qResults[i].getValue('internalid'), 'custrecord_bsp_hsp_inb_q_processed', 'T');
					var remainingUsage = nlapiGetContext().getRemainingUsage();
					if (remainingUsage < 200) nlapiYieldScript();
				}
				catch(e){
					BSP.HSP_ERROR_LOG.createErrorRecord(e);
				}
			}
		}
		catch(e){
			BSP.HSP_ERROR_LOG.createErrorRecord(e);
		}
	},
	__namespace : true
}