/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Sep 2017     Karl Tunberg
 *
 */
if (typeof (BSP) == 'undefined') {
	BSP = {
		__namespace : true
	};
}

BSP.HSP_UTILS = {
	createFieldMapRec : function(prop, recType){
		var mapRec = nlapiCreateRecord('customrecord_bsp_hubspot_field_map');
		mapRec.setFieldValue('custrecord_bsp_hsp_property', prop);
		mapRec.setFieldValue('custrecord_bsp_hsp_netsuite_rec_type', recType);
		nlapiSubmitRecord(mapRec, false, true);
	},
	createInboundQRec : function(recType, data, timestamp){
		var properties = data.properties;
		var qRec = nlapiCreateRecord('customrecord_bsp_hubspot_inbound_q');
		qRec.setFieldValue('custrecord_bsp_hsp_inb_q_rec_type', recType);
		qRec.setFieldValue('custrecord_bsp_hsp_hubspot_data', JSON.stringify(data));
		qRec.setFieldValue('custrecord_bsp_hsp_last_update_stamp', timestamp);
		nlapiSubmitRecord(qRec, false, true);
	},
	createOutQRec : function(action, recType, hubspotId, props, recId){
		var qRec = nlapiCreateRecord('customrecord_bsp_hubspot_out_q');
		qRec.setFieldText('custrecord_bsp_hsp_out_q_action', action);
		qRec.setFieldValue('custrecord_bsp_hsp_out_q_hubspot_id', hubspotId);
		qRec.setFieldValue('custrecord_bsp_hsp_out_q_rec_type', 1);
		if (recType == 'contact')qRec.setFieldValue('custrecord_bsp_hsp_out_q_rec_type', 2);
		qRec.setFieldValue('custrecord_bsp_hsp_out_q_update_props_ob', JSON.stringify(props));
		var timestamp = (new Date().getTime()).toString();
		qRec.setFieldValue('custrecord_bsp_hsp_out_q_timestamp', timestamp);
		qRec.setFieldValue('custrecord_bsp_hsp_out_q_rec_id', recId);
		var qRecId = nlapiSubmitRecord(qRec, false, true);
		return qRecId;
	},
	getQRecs : function(type){
		var filters = [new nlobjSearchFilter('custrecord_bsp_hsp_inb_q_processed', null, 'is', 'F')];
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filters.push(new nlobjSearchFilter('custrecord_bsp_hsp_inb_q_rec_type', null, 'anyof', type));
		var columns = [new nlobjSearchColumn('internalid')];
		columns.push(new nlobjSearchColumn('custrecord_bsp_hsp_hubspot_data'));
		var qResults = nlapiSearchRecord('customrecord_bsp_hubspot_inbound_q', null, filters, columns)
		return qResults;
	},
	getMapFieldProps : function(recType){
		var mapProps = [];
		var filters = [new nlobjSearchFilter('isinactive', null, 'is', 'F')];
		filters.push(new nlobjSearchFilter('custrecord_bsp_hsp_property', null, 'isnotempty'));
		filters.push(new nlobjSearchFilter('custrecord_bsp_hsp_netsuite_rec_type', null, 'anyof', recType));
		var columns = [new nlobjSearchColumn('custrecord_bsp_hsp_property')];
		var mapRecResults = nlapiSearchRecord('customrecord_bsp_hubspot_field_map',null, filters, columns);
		if (!mapRecResults) return mapProps;
		for (var i = 0; i < mapRecResults. length; i++){
			var prop = mapRecResults[i].getValue(columns[0]);
			mapProps.push(prop);
		}
		return mapProps;
	},
	getMapFields : function(recType, nsToHubspot, hubspotToNS){
		var filters = [new nlobjSearchFilter('isinactive', null, 'is', 'F')];
		filters.push(new nlobjSearchFilter('custrecord_bsp_hsp_property', null, 'isnotempty'));
		filters.push(new nlobjSearchFilter('custrecord_bsp_hsp_netsuite_fld_id', null, 'isnotempty'));
		filters.push(new nlobjSearchFilter('custrecord_bsp_hsp_netsuite_rec_type', null, 'anyof', recType));
		if (nsToHubspot)filters.push(new nlobjSearchFilter('custrecord_bsp_hsp_update_in_netsuite', null, 'is', 'T'));
		if (hubspotToNS)filters.push(new nlobjSearchFilter('custrecord_bsp_hsp_update_in_hubspot', null, 'is', 'T'));
		var columns = [new nlobjSearchColumn('custrecord_bsp_hsp_property')];
		columns.push(new nlobjSearchColumn('custrecord_bsp_hsp_netsuite_fld_id'));
		var mapRecResults = nlapiSearchRecord('customrecord_bsp_hubspot_field_map',null, filters, columns);
		return mapRecResults;
	},
	findCompanyByHubspotId : function(id){
		var filters = [new nlobjSearchFilter('custentity_bsp_hubspot_hubspot_id', null, 'is', id)];
		var columns = [new nlobjSearchColumn('internalid')];
		var results = nlapiSearchRecord('customer', null, filters, columns);
		if (!results)return null;
		return results[0].getValue(columns[0]);
	},
	findContactByHubspotId : function(id){
		var filters = [new nlobjSearchFilter('custentity_bsp_hubspot_hubspot_id', null, 'is', id)];
		var columns = [new nlobjSearchColumn('internalid')];
		var results = nlapiSearchRecord('contact', null, filters, columns);
		if (!results)return null;
		return results[0].getValue(columns[0]);
	},
	updateNetsuiteCompany : function(companyId, hubspotId, fieldMaps, qData){
		var settings = BSP.HSP_SETTINGS.getActiveSettings();
		var companyRec;
		var isDeleted = qData.isDeleted;
		if (isDeleted){
			if (settings.deleteInNetsuite == 'F')return;
			if (companyId)nlapiDeleteRecord('customer', companyId);
			return;
		}
		if (companyId){
			if (settings.updateInNetsuite == 'F') return;
			companyRec = nlapiLoadRecord('customer', companyId);
		}
		else {
			if (settings.createInNetsuite == 'F') return;
			companyRec = nlapiCreateRecord('customer');
			companyRec.setFieldValue('custentity_bsp_hubspot_hubspot_id', hubspotId);
		}
		companyRec.setFieldValue('custentity_bsp_hubspot_from_hubspot', 'T');
		var props = qData.properties;
		var subrecProps = {};
		for (var prop in props){
			var value = props[prop].value;
			for (var j = 0; j < fieldMaps.length; j++){
				if (prop == fieldMaps[j].hubspotId){
					var nsFieldType = companyRec.getField(fieldMaps[j].netsuiteId).getType();
					if (fieldMaps[j].netsuiteId.indexOf('bill') == 0){
						subrecProps[prop] = props[prop];
						break;
					}
					companyRec.setFieldValue(fieldMaps[j].netsuiteId, props[prop].value);
					break;
				}
			}
		}
		if (Object.keys(subrecProps).length > 0) companyRec = this.updateSubrecords(companyRec, subrecProps, hubspotId, fieldMaps);
		return nlapiSubmitRecord(companyRec, false, true);
	},
	updateNetsuiteContact : function(contactId, hubspotId, fieldMaps, qData){
		var settings = BSP.HSP_SETTINGS.getActiveSettings();
		var contactRec;
		if (contactId){
			if (settings.updateInNetsuite == 'F') return;
			contactRec = nlapiLoadRecord('contact', contactId);
		}
		else {
			if (settings.createInNetsuite == 'F') return;
			contactRec = nlapiCreateRecord('contact');
			contactRec.setFieldValue('custentity_bsp_hubspot_hubspot_id', hubspotId);
		}
		contactRec.setFieldValue('custentity_bsp_hubspot_from_hubspot', 'T');
		var props = qData.properties;
		var subrecProps = {};
		for (var prop in props){
			var value = props[prop].value;
			for (var j = 0; j < fieldMaps.length; j++){
				if (prop == fieldMaps[j].hubspotId){
					var nsFieldType = contactRec.getField(fieldMaps[j].netsuiteId).getType();
					if (fieldMaps[j].netsuiteId.indexOf('bill') == 0){
						subrecProps[prop] = props[prop];
						break;
					}
					contactRec.setFieldValue(fieldMaps[j].netsuiteId, props[prop].value);
					break;
				}
			}
		}
		var companyRecId = null;
		if (props.hasOwnProperty('associatedcompanyid')) {
			var companyId = props['associatedcompanyid']['value'].toString();
			if (companyId.length > 0)companyRecId = this.findCompanyByHubspotId(companyId);
		}
		if (companyRecId)contactRec.setFieldValue('company', companyRecId);
		if (Object.keys(subrecProps).length > 0) contactRec = this.updateSubrecords(contactRec, subrecProps, hubspotId, fieldMaps);
		return nlapiSubmitRecord(contactRec, false, true);
	},
	updateSubrecords : function(record, props, hubspotId, mapFields){
			record.selectNewLineItem('addressbook');
			record.setCurrentLineItemValue('addressbook', 'defaultbilling', 'T');  //This field is not a subrecord field.
			var subrecord = record.createCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
			var subMaps = [];
			var countryProp = null;
			for (var m = 0; m < mapFields.length; m++){
				if (mapFields[m].netsuiteId == 'billcountry'){
					countryProp = mapFields[m];
					continue;
				}
				if (mapFields[m].netsuiteId.indexOf('bill') == 0){
					subMaps.push(mapFields[m]);
				}
			}
			var response;
			if (record.getRecordType() == 'customer'){
				var response = BSP.HSP_API.getACompany(hubspotId);
			}
			else {
				var response = BSP.HSP_API.getContactById(hubspotId);
			}
			var allProps = JSON.parse(response.getBody())['properties'];
			if (countryProp && allProps.hasOwnProperty(countryProp.hubspotId)){
				var country = allProps[countryProp.hubspotId]['value'];
				var countryCode = 'US';
				if (country){
					countryCode = this.getCountryCode(country);
				}
				subrecord.setFieldValue('country', countryCode);
			}
			var removeMaps = [];
			for (var prop in props){
				for (var s = 0; s < subMaps.length; s++){
					if (subMaps[s].hubspotId == prop){
						if (subMaps[s].netsuiteId == 'billstate'){
							var stateValue = props[prop]['value'];
							var stateAbbr = null;
							if (stateValue) stateAbbr = this.getStateAbbr(stateValue);
							if (stateAbbr) subrecord.setFieldValue('dropdownstate', stateAbbr);
							else {
								if (stateValue && stateValue.length > 0) subrecord.setFieldValue(subMaps[s].netsuiteId.replace('bill', ''), props[prop]['value']);
							}
							continue;
						}
						subrecord.setFieldValue(subMaps[s].netsuiteId.replace('bill', ''), props[prop]['value']);
						removeMaps.push(prop);
					}
				}
			}
			for (var r = 0; r < removeMaps.length; r++){
				for (var y = 0; y < subMaps.length; y++){
					if (subMaps[y].hubspotId == removeMaps[r]){
						subMaps.splice(y, 1);
					}
				}
			}
			if (subMaps.length > 0){
				for (var x = 0; x < subMaps.length; x++){
					for (var key in allProps){
						if (key != subMaps[x].hubspotId)continue;
						if (subMaps[x].netsuiteId == 'billstate'){
							var stateValue = allProps[key]['value'];
							var stateAbbr = null;
							if (stateValue) stateAbbr = this.getStateAbbr(stateValue);
							if (stateAbbr) subrecord.setFieldValue('dropdownstate', stateAbbr);
							else {
								if (stateValue && stateValue.length > 0) subrecord.setFieldValue(subMaps[x].netsuiteId.replace('bill', ''), stateValue);
							}
							continue;
						}
						subrecord.setFieldValue(subMaps[x].netsuiteId.replace('bill', ''), allProps[key].value);
					}
				}
			}
		subrecord.commit();
		record.commitLineItem('addressbook');
		return record;
	},
	addObjects : function(recType, offset, response, timeOffset, paramstamp){
		var results;
		if (recType == 2){
			results = JSON.parse(response.getBody()).contacts;
			while (offset && timeOffset){
				var response = BSP.HSP_API.getRecentlyModifiedContacts(offset, timeOffset);
				var contacts = JSON.parse(response.getBody()).contacts;
				for (var i = 0; i < contacts.length; i++){
					results.push(contacts[i]);
				}
				var lastRecStamp = contacts[contacts.length - 1]['properties']['lastmodifieddate']['value'];
				offset = null;
				timeOffset = null;
				var hasMore = JSON.parse(response.getBody())['has-more'];
				if (hasMore && parseFloat(lastRecStamp) > parseFloat(paramstamp)){
					offset = JSON.parse(response.getBody())['vid-offset'].toString();
					timeOffset =  JSON.parse(response.getBody())['time-offset'].toString();
				}
			}
		}
		else {
			results = JSON.parse(response.getBody()).results;
			while (offset){
				var response = BSP.HSP_API.getRecentlyModifiedCompanies(offset);
				var companies = JSON.parse(response.getBody()).results;
				for (var i = 0; i < companies.length; i++){
					results.push(companies[i]);
				}
				var lastRecStamp = companies[companies.length - 1]['properties']['hs_lastmodifieddate']['value'];
				offset = null;
				var hasMore = JSON.parse(response.getBody())['hasMore'];
				if (hasMore && parseFloat(lastRecStamp) > parseFloat(paramstamp)){
					offset = JSON.parse(response.getBody())['offset'].toString();
				}
			}
		}
		return results;
	},
	getOutboundQRecs : function(recType){
		var filters = [new nlobjSearchFilter('custrecord_bsp_hsp_out_q_rec_type', null, 'anyof', recType)];
		filters.push(new nlobjSearchFilter('custrecord_bsp_hsp_out_q_processed', null, 'is', 'F'));
		var columns = [new nlobjSearchColumn('internalid')];
		columns.push(new nlobjSearchColumn('custrecord_bsp_hsp_out_q_action'));
		columns.push(new nlobjSearchColumn('custrecord_bsp_hsp_out_q_update_props_ob'));
		columns.push(new nlobjSearchColumn('custrecord_bsp_hsp_out_q_hubspot_id'));
		columns.push(new nlobjSearchColumn('custrecord_bsp_hsp_out_q_timestamp'));
		columns.push(new nlobjSearchColumn('custrecord_bsp_hsp_out_q_attempts'));
		columns.push(new nlobjSearchColumn('custrecord_bsp_hsp_out_q_rec_id'));
		columns.push(new nlobjSearchColumn('custrecord_bsp_hsp_out_q_rec_type'));
		columns[7].setSort();
		var results = nlapiSearchRecord('customrecord_bsp_hubspot_out_q', null, filters, columns);
		return results;
	},
	findHubspotCompanyInModified : function(companyId, results){
		var index = -1;
		for (var i = 0; i < results.length; i++){
			var resultsId = results[i].companyId;
			if (parseInt(companyId) == parseInt(resultsId)) index = i;
			break;
		}
		return index;
	},
	removeOutdatedPropertiesInbound : function(inboundData, recType, hubspotId, recId){
		var recordType = 'customer';
		if (recType == 2) recordType = 'contact';
		var sysNotes = this.getSystemNotesAfter(recordType, recId);
		var inProps = inboundData.properties; 
		if (sysNotes.length == 0)return inboundData;
		for (var i = 0; i < sysNotes.length; i++){
			for (var prop in inProps){
				if (prop != sysNotes[i].field)continue;
				var inStamp = inProps[prop].timestamp;
				if (parseFloat(sysNotes[i].timestamp) <= parseFloat(inStamp))continue;
				delete inboundData.properties[prop];
			}
		}
		return inboundData;
	},
	getLatestPropVersion : function(versions){
		var propStamp = 0;
		var version = 0;
		for (var v = 0; v < versions.length; v++){
			var versionStamp = versions[v].timestamp;
			if (parseFloat(versionStamp) > parseFloat(propStamp)){
				propStamp = versionStamp;
				version = v;
			}
		}
		return {
			version		:v,
			timestamp	:propStamp
		};
	},
	getOutPostData : function(action, recId, recType, qData, postData, hubResults, qStamp){
		var qData = null;
		if (recType == 1)var resultsIndex = BSP.HSP_UTILS.findHubspotCompanyInModified(recId, hubResults);
		if (recType == 2){
			var contResponse = BSP.HSP_API.getContactById(recId);
			var contObj = JSON.parse(contResponse.getBody());
		}
		if (!qData)return null;
		for (var prop in qData){
			if (action == 'UPDATE' && resultsIndex > -1){
				if (recType == 1) var modifiedProps = hubResults[resultsIndex].properties;
				else var modifiedProps = contObj.properties;
				var modifiedStamp = 0;
				
				if (modifiedProps.hasOwnProperty(prop)){
					
					if (recType == 1) modifiedStamp = modifiedProps[prop].timestamp;
					else {
						var latestVersion = BSP.HSP_UTILS.getLatestPropVersion(modifiedProps[prop].versions)
						modifiedStamp = latestVersion.timestamp;
					}
				}
				if (parseFloat(modifiedStamp) >= parseFloat(qStamp))continue;
			}
			postData.properties.push({
				name	:prop,
				value	:qData[prop]
			});
		}
		return postData;
	},
	getContactPostData : function(hubspotId, qData, action, qStamp){
		var postData = {'properties' : []};
		//var properties = contactObj.properties;
		for (var prop in qData){
			if (action == 'UPDATE'){
				var response = BSP.HSP_API.getContactById(hubspotId);
				var contactObj = JSON.parse(response.getBody());
				var modifiedProps =  contactObj.properties;
				var modifiedStamp = 0;
				
				if (modifiedProps.hasOwnProperty(prop)){
					modifiedStamp = this.getLatestPropVersion(modifiedProps[prop].versions).timestamp;
				}
				if (parseFloat(modifiedStamp) >= parseFloat(qStamp))continue;
			}
			postData.properties.push({
				property	:prop,
				value		:qData[prop]
			});
		}
		return postData;
	},
	processHubspotModifiedContacts : function(results, paramstamp, timestamp, settings, mapArray, i){
		var lastTimestamp = results[i].properties['lastmodifieddate'].value; 
		var diff = (parseFloat(paramstamp) - parseFloat(lastTimestamp)).toFixed(0);
		var contactId = results[i]['vid'];
		var contactResponse = BSP.HSP_API.getContactById(contactId);
		var conData = JSON.parse(contactResponse.getBody());
		var props = conData.properties;
		for (var prop in props){
			var versions = props[prop].versions;
			var lastVersion = BSP.HSP_UTILS.getLatestPropVersion(versions);
			props[prop].timestamp = lastVersion.timestamp;
			delete props[prop].versions;
			if (parseFloat(lastVersion.timestamp) <= parseFloat(paramstamp) || mapArray.indexOf(prop) < 0){
				if (prop!= 'associatedcompanyid') {
					delete props[prop];
				}
			}
		}
		return {
			lastTimestamp	: lastTimestamp,
			data			: conData
		}
	},
	processHubspotModifiedCompanies : function(results, paramstamp, mapArray, i){
		var props = results[i].properties;
		var lastModified = props.hs_lastmodifieddate;
		var lastTimestamp = lastModified.timestamp;
		for (var prop in props){
			var propstamp = props[prop].timestamp;
			if (propstamp < paramstamp || mapArray.indexOf(prop) < 0){
				delete props[prop];
			}
		}
		return {
			lastTimestamp	: lastTimestamp,
			data			: results[i]
		}
	},
	getSystemNotesAfter : function(type, recId){
		var sysNotes = [];
		if (!recId)return sysNotes;
		var settings = BSP.HSP_SETTINGS.getActiveSettings();
		if (!settings)throw nlapiCreateError('NO_SETTINGS', 'No Settings found', true);
		var today = new Date();
		var date = nlapiDateToString(today, 'datetime');
		if (type == 'customer')var stamp = settings.lastCtmrInQ;
		else stamp = settings.lastContInQ;
		if (stamp){
			today = new Date(parseFloat(stamp));
			date = nlapiDateToString(today, 'datetime')
		}
		var results = nlapiSearchRecord(type,null,
			[
			   ["systemnotes.date","after", date], 
			   "AND", 
			   ["systemnotes.type","is","F"],
			   "AND",
			   ["internalid", 'anyof', recId]
			], 
			[
			   new nlobjSearchColumn("field","systemNotes",null), 
			   new nlobjSearchColumn("oldvalue","systemNotes",null), 
			   new nlobjSearchColumn("newvalue","systemNotes",null),
			   new nlobjSearchColumn("date","systemNotes",null)
			]
		);
		if (!results) return sysNotes;
		for (var i = 0; i < results.length; i++){
			var columns = results[i].getAllColumns();
			sysNotes.push({		
				field 		: results[i].getValue(columns[0]),
				timestamp	: nlapiStringToDate(results[i].getValue(columns[3]), 'datetime').getTime()
			});
		}
		return sysNotes;
	},
	states : [{
		    "name": "Alabama",
		        "abbreviation": "AL"
		}, {
		    "name": "Alaska",
		        "abbreviation": "AK"
		}, {
		    "name": "American Samoa",
		        "abbreviation": "AS"
		}, {
		    "name": "Arizona",
		        "abbreviation": "AZ"
		}, {
		    "name": "Arkansas",
		        "abbreviation": "AR"
		}, {
		    "name": "British Columbia",
		        "abbreviation": "BC"
		}, {
		    "name": "California",
		        "abbreviation": "CA"
		}, {
		    "name": "Colorado",
		        "abbreviation": "CO"
		}, {
		    "name": "Connecticut",
		        "abbreviation": "CT"
		}, {
		    "name": "Delaware",
		        "abbreviation": "DE"
		}, {
		    "name": "District Of Columbia",
		        "abbreviation": "DC"
		}, {
		    "name": "Federated States Of Micronesia",
		        "abbreviation": "FM"
		}, {
		    "name": "Florida",
		        "abbreviation": "FL"
		}, {
		    "name": "Georgia",
		        "abbreviation": "GA"
		}, {
		    "name": "Guam",
		        "abbreviation": "GU"
		}, {
		    "name": "Hawaii",
		        "abbreviation": "HI"
		}, {
		    "name": "Idaho",
		        "abbreviation": "ID"
		}, {
		    "name": "Illinois",
		        "abbreviation": "IL"
		}, {
		    "name": "Indiana",
		        "abbreviation": "IN"
		}, {
		    "name": "Iowa",
		        "abbreviation": "IA"
		}, {
		    "name": "Kansas",
		        "abbreviation": "KS"
		}, {
		    "name": "Kentucky",
		        "abbreviation": "KY"
		}, {
		    "name": "Louisiana",
		        "abbreviation": "LA"
		}, {
		    "name": "Maine",
		        "abbreviation": "ME"
		}, {
		    "name": "Manitoba",
		        "abbreviation": "MB"
		}, {
		    "name": "Marshall Islands",
		        "abbreviation": "MH"
		}, {
		    "name": "Maryland",
		        "abbreviation": "MD"
		}, {
		    "name": "Massachusetts",
		        "abbreviation": "MA"
		}, {
		    "name": "Michigan",
		        "abbreviation": "MI"
		}, {
		    "name": "Minnesota",
		        "abbreviation": "MN"
		}, {
		    "name": "Mississippi",
		        "abbreviation": "MS"
		}, {
		    "name": "Missouri",
		        "abbreviation": "MO"
		}, {
		    "name": "Montana",
		        "abbreviation": "MT"
		}, {
		    "name": "Nebraska",
		        "abbreviation": "NE"
		}, {
		    "name": "Nevada",
		        "abbreviation": "NV"
		}, {
		    "name": "New Brunswick",
		        "abbreviation": "NB"
		}, {
		    "name": "New Hampshire",
		        "abbreviation": "NH"
		}, {
		    "name": "New Jersey",
		        "abbreviation": "NJ"
		}, {
		    "name": "New Mexico",
		        "abbreviation": "NM"
		}, {
		    "name": "New York",
		        "abbreviation": "NY"
		}, {
		    "name": "Newfoundland and Labrador",
		        "abbreviation": "NL"
		}, {
		    "name": "North Carolina",
		        "abbreviation": "NC"
		}, {
		    "name": "North Dakota",
		        "abbreviation": "ND"
		}, {
		    "name": "Northern Mariana Islands",
		        "abbreviation": "MP"
		}, {
		    "name": "Nova Scotia",
		        "abbreviation": "NS"
		}, {
		    "name": "Northwest Territories",
		        "abbreviation": "NT"
		}, {
		    "name": "Nunavut",
		        "abbreviation": "NU"
		}, {
		    "name": "Ohio",
		        "abbreviation": "OH"
		}, {
		    "name": "Oklahoma",
		        "abbreviation": "OK"
		}, {
		    "name": "Ontario",
		        "abbreviation": "ON"
		}, {
		    "name": "Oregon",
		        "abbreviation": "OR"
		}, {
		    "name": "Palau",
		        "abbreviation": "PW"
		}, {
		    "name": "Pennsylvania",
		        "abbreviation": "PA"
		}, {
		    "name": "Prince Edward Island",
		        "abbreviation": "PE"
		}, {
		    "name": "Puerto Rico",
		        "abbreviation": "PR"
		}, {
		    "name": "Quebec",
		        "abbreviation": "QC"
		}, {
		    "name": "Rhode Island",
		        "abbreviation": "RI"
		}, {
		    "name": "Saskatchewan",
		        "abbreviation": "SK"
		}, {
		    "name": "South Carolina",
		        "abbreviation": "SC"
		}, {
		    "name": "South Dakota",
		        "abbreviation": "SD"
		}, {
		    "name": "Tennessee",
		        "abbreviation": "TN"
		}, {
		    "name": "Texas",
		        "abbreviation": "TX"
		}, {
		    "name": "Utah",
		        "abbreviation": "UT"
		}, {
		    "name": "Vermont",
		        "abbreviation": "VT"
		}, {
		    "name": "Virgin Islands",
		        "abbreviation": "VI"
		}, {
		    "name": "Virginia",
		        "abbreviation": "VA"
		}, {
		    "name": "Washington",
		        "abbreviation": "WA"
		}, {
		    "name": "West Virginia",
		        "abbreviation": "WV"
		}, {
		    "name": "Wisconsin",
		        "abbreviation": "WI"
		}, {
		    "name": "Wyoming",
		        "abbreviation": "WY"
		}, {
		    "name": "Yukon",
		        "abbreviation": "YT"
		}],
	      getStateAbbr : function(name){
	    	  var abbr = null;
	    	  var states = this.states;
	    	  if (name.length == 2){
	    		  for (var a = 0 ; a < states.length; a++){
	    			  if (states[a].abbreviation == name.toUpperCase()){
	    				  return name.toUpperCase();
	    			  }
	    		  }
	    	  }
	    	  var lowerName = name.toLowerCase();
	    	  for (var i = 0; i <states.length; i++){
	    		  var objName = states[i]['name'];
	    		  var lowerObjName = objName.toLowerCase();
	    		  if (lowerName == lowerObjName){ 
	    			  abbr = states[i]['abbreviation'];
	    			  break;
	    		  }
	    	  }
	    	  return abbr.toUpperCase();
	      },
		  	getStateName : function(code){
		  		
		  	},
	      countryCodes : [{"Name":"Afghanistan","Code":"AF"},{"Name":"�land Islands","Code":"AX"},{"Name":"Albania","Code":"AL"},{"Name":"Algeria","Code":"DZ"},{"Name":"American Samoa","Code":"AS"},{"Name":"Andorra","Code":"AD"},{"Name":"Angola","Code":"AO"},{"Name":"Anguilla","Code":"AI"},{"Name":"Antarctica","Code":"AQ"},{"Name":"Antigua and Barbuda","Code":"AG"},{"Name":"Argentina","Code":"AR"},{"Name":"Armenia","Code":"AM"},{"Name":"Aruba","Code":"AW"},{"Name":"Australia","Code":"AU"},{"Name":"Austria","Code":"AT"},{"Name":"Azerbaijan","Code":"AZ"},{"Name":"Bahamas","Code":"BS"},{"Name":"Bahrain","Code":"BH"},{"Name":"Bangladesh","Code":"BD"},{"Name":"Barbados","Code":"BB"},{"Name":"Belarus","Code":"BY"},{"Name":"Belgium","Code":"BE"},{"Name":"Belize","Code":"BZ"},{"Name":"Benin","Code":"BJ"},{"Name":"Bermuda","Code":"BM"},{"Name":"Bhutan","Code":"BT"},{"Name":"Bolivia, Plurinational State of","Code":"BO"},{"Name":"Bonaire, Sint Eustatius and Saba","Code":"BQ"},{"Name":"Bosnia and Herzegovina","Code":"BA"},{"Name":"Botswana","Code":"BW"},{"Name":"Bouvet Island","Code":"BV"},{"Name":"Brazil","Code":"BR"},{"Name":"British Indian Ocean Territory","Code":"IO"},{"Name":"Brunei Darussalam","Code":"BN"},{"Name":"Bulgaria","Code":"BG"},{"Name":"Burkina Faso","Code":"BF"},{"Name":"Burundi","Code":"BI"},{"Name":"Cambodia","Code":"KH"},{"Name":"Cameroon","Code":"CM"},{"Name":"Canada","Code":"CA"},{"Name":"Cape Verde","Code":"CV"},{"Name":"Cayman Islands","Code":"KY"},{"Name":"Central African Republic","Code":"CF"},{"Name":"Chad","Code":"TD"},{"Name":"Chile","Code":"CL"},{"Name":"China","Code":"CN"},{"Name":"Christmas Island","Code":"CX"},{"Name":"Cocos (Keeling) Islands","Code":"CC"},{"Name":"Colombia","Code":"CO"},{"Name":"Comoros","Code":"KM"},{"Name":"Congo","Code":"CG"},{"Name":"Congo, the Democratic Republic of the","Code":"CD"},{"Name":"Cook Islands","Code":"CK"},{"Name":"Costa Rica","Code":"CR"},{"Name":"C�te d'Ivoire","Code":"CI"},{"Name":"Croatia","Code":"HR"},{"Name":"Cuba","Code":"CU"},{"Name":"Cura�ao","Code":"CW"},{"Name":"Cyprus","Code":"CY"},{"Name":"Czech Republic","Code":"CZ"},{"Name":"Denmark","Code":"DK"},{"Name":"Djibouti","Code":"DJ"},{"Name":"Dominica","Code":"DM"},{"Name":"Dominican Republic","Code":"DO"},{"Name":"Ecuador","Code":"EC"},{"Name":"Egypt","Code":"EG"},{"Name":"El Salvador","Code":"SV"},{"Name":"Equatorial Guinea","Code":"GQ"},{"Name":"Eritrea","Code":"ER"},{"Name":"Estonia","Code":"EE"},{"Name":"Ethiopia","Code":"ET"},{"Name":"Falkland Islands (Malvinas)","Code":"FK"},{"Name":"Faroe Islands","Code":"FO"},{"Name":"Fiji","Code":"FJ"},{"Name":"Finland","Code":"FI"},{"Name":"France","Code":"FR"},{"Name":"French Guiana","Code":"GF"},{"Name":"French Polynesia","Code":"PF"},{"Name":"French Southern Territories","Code":"TF"},{"Name":"Gabon","Code":"GA"},{"Name":"Gambia","Code":"GM"},{"Name":"Georgia","Code":"GE"},{"Name":"Germany","Code":"DE"},{"Name":"Ghana","Code":"GH"},{"Name":"Gibraltar","Code":"GI"},{"Name":"Greece","Code":"GR"},{"Name":"Greenland","Code":"GL"},{"Name":"Grenada","Code":"GD"},{"Name":"Guadeloupe","Code":"GP"},{"Name":"Guam","Code":"GU"},{"Name":"Guatemala","Code":"GT"},{"Name":"Guernsey","Code":"GG"},{"Name":"Guinea","Code":"GN"},{"Name":"Guinea-Bissau","Code":"GW"},{"Name":"Guyana","Code":"GY"},{"Name":"Haiti","Code":"HT"},{"Name":"Heard Island and McDonald Islands","Code":"HM"},{"Name":"Holy See (Vatican City State)","Code":"VA"},{"Name":"Honduras","Code":"HN"},{"Name":"Hong Kong","Code":"HK"},{"Name":"Hungary","Code":"HU"},{"Name":"Iceland","Code":"IS"},{"Name":"India","Code":"IN"},{"Name":"Indonesia","Code":"ID"},{"Name":"Iran, Islamic Republic of","Code":"IR"},{"Name":"Iraq","Code":"IQ"},{"Name":"Ireland","Code":"IE"},{"Name":"Isle of Man","Code":"IM"},{"Name":"Israel","Code":"IL"},{"Name":"Italy","Code":"IT"},{"Name":"Jamaica","Code":"JM"},{"Name":"Japan","Code":"JP"},{"Name":"Jersey","Code":"JE"},{"Name":"Jordan","Code":"JO"},{"Name":"Kazakhstan","Code":"KZ"},{"Name":"Kenya","Code":"KE"},{"Name":"Kiribati","Code":"KI"},{"Name":"Korea, Democratic People's Republic of","Code":"KP"},{"Name":"Korea, Republic of","Code":"KR"},{"Name":"Kuwait","Code":"KW"},{"Name":"Kyrgyzstan","Code":"KG"},{"Name":"Lao People's Democratic Republic","Code":"LA"},{"Name":"Latvia","Code":"LV"},{"Name":"Lebanon","Code":"LB"},{"Name":"Lesotho","Code":"LS"},{"Name":"Liberia","Code":"LR"},{"Name":"Libya","Code":"LY"},{"Name":"Liechtenstein","Code":"LI"},{"Name":"Lithuania","Code":"LT"},{"Name":"Luxembourg","Code":"LU"},{"Name":"Macao","Code":"MO"},{"Name":"Macedonia, the Former Yugoslav Republic of","Code":"MK"},{"Name":"Madagascar","Code":"MG"},{"Name":"Malawi","Code":"MW"},{"Name":"Malaysia","Code":"MY"},{"Name":"Maldives","Code":"MV"},{"Name":"Mali","Code":"ML"},{"Name":"Malta","Code":"MT"},{"Name":"Marshall Islands","Code":"MH"},{"Name":"Martinique","Code":"MQ"},{"Name":"Mauritania","Code":"MR"},{"Name":"Mauritius","Code":"MU"},{"Name":"Mayotte","Code":"YT"},{"Name":"Mexico","Code":"MX"},{"Name":"Micronesia, Federated States of","Code":"FM"},{"Name":"Moldova, Republic of","Code":"MD"},{"Name":"Monaco","Code":"MC"},{"Name":"Mongolia","Code":"MN"},{"Name":"Montenegro","Code":"ME"},{"Name":"Montserrat","Code":"MS"},{"Name":"Morocco","Code":"MA"},{"Name":"Mozambique","Code":"MZ"},{"Name":"Myanmar","Code":"MM"},{"Name":"Namibia","Code":"NA"},{"Name":"Nauru","Code":"NR"},{"Name":"Nepal","Code":"NP"},{"Name":"Netherlands","Code":"NL"},{"Name":"New Caledonia","Code":"NC"},{"Name":"New Zealand","Code":"NZ"},{"Name":"Nicaragua","Code":"NI"},{"Name":"Niger","Code":"NE"},{"Name":"Nigeria","Code":"NG"},{"Name":"Niue","Code":"NU"},{"Name":"Norfolk Island","Code":"NF"},{"Name":"Northern Mariana Islands","Code":"MP"},{"Name":"Norway","Code":"NO"},{"Name":"Oman","Code":"OM"},{"Name":"Pakistan","Code":"PK"},{"Name":"Palau","Code":"PW"},{"Name":"Palestine, State of","Code":"PS"},{"Name":"Panama","Code":"PA"},{"Name":"Papua New Guinea","Code":"PG"},{"Name":"Paraguay","Code":"PY"},{"Name":"Peru","Code":"PE"},{"Name":"Philippines","Code":"PH"},{"Name":"Pitcairn","Code":"PN"},{"Name":"Poland","Code":"PL"},{"Name":"Portugal","Code":"PT"},{"Name":"Puerto Rico","Code":"PR"},{"Name":"Qatar","Code":"QA"},{"Name":"R�union","Code":"RE"},{"Name":"Romania","Code":"RO"},{"Name":"Russian Federation","Code":"RU"},{"Name":"Rwanda","Code":"RW"},{"Name":"Saint Barth�lemy","Code":"BL"},{"Name":"Saint Helena, Ascension and Tristan da Cunha","Code":"SH"},{"Name":"Saint Kitts and Nevis","Code":"KN"},{"Name":"Saint Lucia","Code":"LC"},{"Name":"Saint Martin (French part)","Code":"MF"},{"Name":"Saint Pierre and Miquelon","Code":"PM"},{"Name":"Saint Vincent and the Grenadines","Code":"VC"},{"Name":"Samoa","Code":"WS"},{"Name":"San Marino","Code":"SM"},{"Name":"Sao Tome and Principe","Code":"ST"},{"Name":"Saudi Arabia","Code":"SA"},{"Name":"Senegal","Code":"SN"},{"Name":"Serbia","Code":"RS"},{"Name":"Seychelles","Code":"SC"},{"Name":"Sierra Leone","Code":"SL"},{"Name":"Singapore","Code":"SG"},{"Name":"Sint Maarten (Dutch part)","Code":"SX"},{"Name":"Slovakia","Code":"SK"},{"Name":"Slovenia","Code":"SI"},{"Name":"Solomon Islands","Code":"SB"},{"Name":"Somalia","Code":"SO"},{"Name":"South Africa","Code":"ZA"},{"Name":"South Georgia and the South Sandwich Islands","Code":"GS"},{"Name":"South Sudan","Code":"SS"},{"Name":"Spain","Code":"ES"},{"Name":"Sri Lanka","Code":"LK"},{"Name":"Sudan","Code":"SD"},{"Name":"Suriname","Code":"SR"},{"Name":"Svalbard and Jan Mayen","Code":"SJ"},{"Name":"Swaziland","Code":"SZ"},{"Name":"Sweden","Code":"SE"},{"Name":"Switzerland","Code":"CH"},{"Name":"Syrian Arab Republic","Code":"SY"},{"Name":"Taiwan, Province of China","Code":"TW"},{"Name":"Tajikistan","Code":"TJ"},{"Name":"Tanzania, United Republic of","Code":"TZ"},{"Name":"Thailand","Code":"TH"},{"Name":"Timor-Leste","Code":"TL"},{"Name":"Togo","Code":"TG"},{"Name":"Tokelau","Code":"TK"},{"Name":"Tonga","Code":"TO"},{"Name":"Trinidad and Tobago","Code":"TT"},{"Name":"Tunisia","Code":"TN"},{"Name":"Turkey","Code":"TR"},{"Name":"Turkmenistan","Code":"TM"},{"Name":"Turks and Caicos Islands","Code":"TC"},{"Name":"Tuvalu","Code":"TV"},{"Name":"Uganda","Code":"UG"},{"Name":"Ukraine","Code":"UA"},{"Name":"United Arab Emirates","Code":"AE"},{"Name":"United Kingdom","Code":"GB"},{"Name":"United States","Code":"US"},{"Name":"United States Minor Outlying Islands","Code":"UM"},{"Name":"Uruguay","Code":"UY"},{"Name":"Uzbekistan","Code":"UZ"},{"Name":"Vanuatu","Code":"VU"},{"Name":"Venezuela, Bolivarian Republic of","Code":"VE"},{"Name":"Viet Nam","Code":"VN"},{"Name":"Virgin Islands, British","Code":"VG"},{"Name":"Virgin Islands, U.S.","Code":"VI"},{"Name":"Wallis and Futuna","Code":"WF"},{"Name":"Western Sahara","Code":"EH"},{"Name":"Yemen","Code":"YE"},{"Name":"Zambia","Code":"ZM"},{"Name":"Zimbabwe","Code":"ZW"}],
	  	getCountryCode : function(country){
	  		var countryCode = 'US';
	  		var codes = this.countryCodes;
	  		if (country.length == 2){
	  			for (var j = 0; j < codes.length; j++){
		  			var codesCountry = codes[j]['Code'];
		  			if (country.toLowerCase() == codesCountry.toLowerCase())
		  				countryCode = codes[j]['Code'];
		  			break;
		  		}
	  			return countryCode;
	  		}
	  		for (var i = 0; i < codes.length; i++){
	  			var codesCountry = codes[i]['Name'];
	  			if (country.toLowerCase() == codesCountry.toLowerCase()){
	  				countryCode = codes[i]['Code'];
	  				break;
	  			}
	  		}
	  		return countryCode;
	  	},
	  	getCountryName : function(code){
	  		
	  	},
	__namespace : true
}