/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 Aug 2017     Karl Tunberg
 *
 */

if (typeof (BSP) == 'undefined') {
	BSP = {
		__namespace : true
	};
}

BSP.HSP_SETTINGS = {
	getActiveSettings : function(){
		var filters = [new nlobjSearchFilter('isinactive', null, 'is', 'F')];
		var columns = [new nlobjSearchColumn('internalid')];
		columns.push(new nlobjSearchColumn('created'));
		columns[1].setSort(true);
		var settingResults = nlapiSearchRecord('customrecord_bsp_hubspot_integr_settings', null, filters, columns);
		if (!settingResults)throw nlapiCreateError('MISSING_SETTINGS_RECORD', 'An active Hubspot Settings Integration Record has to be created.', true);
		var settingsId = settingResults[0].getValue(columns[0]);
		var settings = nlapiLoadRecord('customrecord_bsp_hubspot_integr_settings', settingsId);
		return {
			token				:settings.getFieldValue('custrecord_bsp_hubspot_token'),
			tokenTime			:settings.getFieldValue('custrecord_bsp_hubspot_token_retrieval'),
			clientId			:settings.getFieldValue('custrecord_bsp_hubspot_client_id'),
			clientSecret		:settings.getFieldValue('custrecord_bsp_hubspot_client_secret'),
			redirectURI			:settings.getFieldValue('custrecord_bsp_hubspot_redirect_uri'),
			refreshToken		:settings.getFieldValue('custrecord_bsp_hubspot_refresh_token'),
			recordId			:settings.getId(),
			hubspotToNetsuite	:settings.getFieldValue('custrecord_bsp_hubspot_hsp_to_ns'),
			netsuiteToHubspot	:settings.getFieldValue('custrecord_bsp_hubspot_ns_to_hsp'),
			lastToHubSpot		:settings.getFieldValue('custrecord_bsp_hubspot_last_ns_to_hsp'),
			lastCtmrInQ			:settings.getFieldValue('custrecord_bsp_hubspot_last_ctmr_in_q'),
			lastContInQ			:settings.getFieldValue('custrecord_bsp_hubspot_last_cntct_in_q'),
			createInNetsuite	:settings.getFieldValue('custrecord_bsp_hubspot_create_ns_recs'),
			updateInNetsuite	:settings.getFieldValue('custrecord_bsp_hubspot_update_ns_recs'),
			deleteInNetsuite	:settings.getFieldValue('custrecord_bsp_hubspot_delete_ns_recs'),
			createInHubspot		:settings.getFieldValue('custrecord_bsp_hubspot_create_hsp_recs'),
			updateInHubspot		:settings.getFieldValue('custrecord_bsp_hubspot_update_hsp_recs'),
			deleteInHubspot		:settings.getFieldValue('custrecord_bsp_hubspot_delete_hsp_recs')
		};
	},
	isTokenValid : function(settings){
		if (!settings)settings = this.getActiveSettings();
		var tokenTimeStr = settings.tokenTime;
		var currentDate = new Date();
		var nowTime = currentDate.getTime()/1000;
		var tokenTime = 0;
		if( tokenTimeStr)tokenTime = parseFloat(tokenTimeStr);
		if ((nowTime - tokenTime) > 18000) return false;
		return true;
	},
	updateSettingsRec : function(settings, values){
		var settingsRec = nlapiLoadRecord('customrecord_bsp_hubspot_integr_settings', settings.recordId);
		for(var prop in values){
			settingsRec.setFieldValue(prop, values[prop]);
		}
		nlapiSubmitRecord(settingsRec, false, true);
	},
	__namespace : true
}
