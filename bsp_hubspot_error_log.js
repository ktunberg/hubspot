/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 Aug 2017     Karl Tunberg
 *
 */
if (typeof (BSP) == 'undefined') {
	BSP = {
		__namespace : true
	};
}

BSP.HSP_ERROR_LOG = {
	createErrorRecord : function(err){
		var errorRec = nlapiCreateRecord('customrecord_bsp_hubspot_error_log');
		var scriptId = nlapiGetContext().getScriptId();
		var deploymentId = nlapiGetContext().getDeploymentId();
		errorRec.setFieldValue('custrecord_bsp_hubspot_script_id', scriptId);
		errorRec.setFieldValue('custrecord_bsp_hubspot_deployment_id', deploymentId);
		
		if (err instanceof nlobjError){
			errorRec.setFieldValue('custrecord_bsp_hubspot_is_nlobjerror', 'T');
			errorRec.setFieldValue('custrecord_bsp_hubspot_error_code', err.getCode());
			errorRec.setFieldValue('custrecord_bsp_hubspot_error_message', err.getDetails());
			errorRec.setFieldValue('custrecord_bsp_hubspot_stack_trace', err.getStackTrace());
			errorRec.setFieldValue('custrecord_bsp_hubspot_error_id', err.getId());
		}
		else {
			errorRec.setFieldValue('custrecord_bsp_hubspot_error_code', err.name);
			errorRec.setFieldValue('custrecord_bsp_hubspot_error_message', err.message);
		}
		nlapiSubmitRecord(errorRec, false, true);
	},
	__namespace : true
}