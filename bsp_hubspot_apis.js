/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 Aug 2017     Karl Tunberg
 *
 */
if (typeof (BSP) == 'undefined') {
	BSP = {
		__namespace : true
	};
}

BSP.HSP_API = {
	getHeaders : function(){
		var settings = BSP.HSP_SETTINGS.getActiveSettings();
		var headers = {
			'Content-Type'	:'application/json',
			'Accept'		:'application/json'
		}
		if (BSP.HSP_SETTINGS.isTokenValid(settings)){
			headers['Authorization'] = 'Bearer ' + settings.token
			return headers;
		}
		var tokenHeaders = new Object();
		tokenHeaders['Content-Type'] = 'application/x-www-form-urlencoded';
		tokenHeaders['Accept'] = 'application/json';
		tokenHeaders['charset'] = 'utf-8';
		var body = 'grant_type=refresh_token&';
		body += 'client_id=' + settings.clientId + '&';
		body += 'client_secret=' + settings.clientSecret + '&';
		body += 'redirect_uri=' +  encodeURIComponent(settings.redirectURI) + '&';
		body += 'refresh_token=' + settings.refreshToken;
		var url = 'https://api.hubapi.com/oauth/v1/token';
		var response = nlapiRequestURL(url, body, tokenHeaders);
		var responseBody = response.getBody();
		var responseObj = JSON.parse(responseBody);
		var responseCode = response.getCode();
		if (parseFloat(responseCode) > 201){
			throw nlapiCreateError(responseObj.status, responseObj.details, true);
		}
		var token = responseObj.access_token;
		var refreshToken = responseObj.refresh_token;
		var nowDate = new Date();
		var nowTime = nowDate.getTime()/1000;
		var updateValues = {
				custrecord_bsp_hubspot_token			:token,
				custrecord_bsp_hubspot_refresh_token	:refreshToken,
				custrecord_bsp_hubspot_token_retrieval	:nowTime	
		};
		BSP.HSP_SETTINGS.updateSettingsRec(settings, updateValues);
		headers['Authorization'] = 'Bearer ' + token;
		return  headers;
	},
	getRecentlyModifiedCompanies : function(offset){
		var url = 'https://api.hubapi.com/companies/v2/companies/recent/modified?count=10';
		if (offset){
			url += '&offset=' + offset;
		}
		var response = this.callService(url, null, this.getHeaders(), 'GET');
		return response;
	},
	getACompany : function(companyId){
		var url = 'https://api.hubapi.com/companies/v2/companies/' + companyId;
		var response = this.callService(url, null, this.getHeaders(), 'GET');
		return response;
	},
	getRecentlyModifiedContacts : function(offset, timeOffset){
		var url = 'https://api.hubapi.com/contacts/v1/lists/recently_updated/contacts/recent?count=10';
		if (offset && timeOffset){
			url += '&timeOffset' + timeOffset + '&vidOffset=' + offset;
		}
		var response = this.callService(url, null, this.getHeaders(), 'GET');
		return response;
	},
	createACompany : function(postData){
		var url = 'https://api.hubapi.com/companies/v2/companies/';
		var response = this.callService(url, postData, this.getHeaders(), 'POST');
		return response;
	},
	updateACompany : function(postData, companyId){
		var url = 'https://api.hubapi.com/companies/v2/companies/' + companyId;
		var response = this.callService(url, postData, this.getHeaders(), 'PUT');
		return response;
	},
	deleteACompany : function(companyId){
		var url = 'https://api.hubapi.com/companies/v2/companies/' + companyId;
		var response = this.callService(url, null, this.getHeaders(), 'DELETE');
		return response;
	},
	createAContact : function(postData){
		var url = 'https://api.hubapi.com/contacts/v1/contact';
		var response = this.callService(url, postData, this.getHeaders(), 'POST');
		return response;
	},
	updateAContact : function(postData, contactId){
		var url = 'https://api.hubapi.com/contacts/v1/contact/vid/' + contactId + '/profile';
		var response = this.callService(url, postData, this.getHeaders(), 'POST');
		return response;
	},
	deleteAContact : function(contactId){
		var url = 'https://api.hubapi.com/contacts/v1/contact/vid/' + contactId;
		var response = this.callService(url, null, this.getHeaders(), 'DELETE');
		return response;
	},
	getContactById : function(contactId){
		var url = 'https://api.hubapi.com/contacts/v1/contact/vid/' + contactId + '/profile';
		var response = this.callService(url, null, this.getHeaders(), 'GET');
		return response;
	},
	getRecentlyCreatedContacts : function(){
		var url = 'https://api.hubapi.com/contacts/v1/lists/all/contacts/recent?count=10';
		var response = this.callService(url, null, this.getHeaders(), 'GET');
		return response;
	},
	getCompanyProperties : function(){
		var url = 'https://api.hubapi.com/properties/v1/companies/properties/';
		var response = this.callService(url, null, this.getHeaders(), 'GET');
		return response;
	},
	getContactProperties : function(){
		var url = 'https://api.hubapi.com/properties/v1/contacts/properties';
		var response = this.callService(url, null, this.getHeaders(), 'GET');
		return response;
	},
	callService : function(url, postData, headers, method){
		var response = null;
		if (postData) response = nlapiRequestURL(url, JSON.stringify(postData), headers, method);
		else response = nlapiRequestURL(url, null, headers, method);
		var logId = BSP.HSP_SERVICE_LOG.createLog(url, method, postData, headers, response);
		if (response.getCode() > 299)throw nlapiCreateError('API_CALL_ERROR', 'API Call Error', true);
		return response;
	},
	__namespace : true
}

