/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Sep 2017     Karl Tunberg
 *
 */
if (typeof (BSP) == 'undefined') {
	BSP = {
		__namespace : true
	};
}
/**
/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
BSP.HSP_OUT = {
	updateHubspotRecords : function(type){
		try {
			var settings = BSP.HSP_SETTINGS.getActiveSettings();
			if (settings.netsuiteToHubspot == 'F')return;
			var qRecs = BSP.HSP_UTILS.getOutboundQRecs([1,2]);
			if (!qRecs)return;
			var hubResponse = BSP.HSP_API.getRecentlyModifiedCompanies();
			var hubResponseObj = JSON.parse(hubResponse.getBody());
			var hubResults = hubResponseObj.results;
			for (var i = 0; i < qRecs.length; i++){
				try{
					var qData = null;
					var recordType = qRecs[i].getValue('custrecord_bsp_hsp_out_q_rec_type');
					var qId = qRecs[i].getValue('internalid');
					var action = qRecs[i].getText('custrecord_bsp_hsp_out_q_action');
					var qStamp = qRecs[i].getValue('custrecord_bsp_hsp_out_q_timestamp');
					var qString = qRecs[i].getValue('custrecord_bsp_hsp_out_q_update_props_ob');
					if (qString)qData = JSON.parse(qString);
					var postData = {'properties' : []};
					var attempts = qRecs[i].getValue('custrecord_bsp_hsp_out_q_attempts');
					if (attempts && parseInt(attempts) > 4)continue;
					var hubspotId = qRecs[i].getValue('custrecord_bsp_hsp_out_q_hubspot_id');
					var resultsIndex = BSP.HSP_UTILS.findHubspotCompanyInModified(hubspotId, hubResults);
					if (qData){
						if (parseInt(recordType) == 1){
							for (var prop in qData){
								if (action == 'UPDATE' && resultsIndex > -1){
									var modifiedProps = hubResults[resultsIndex].properties;
									var modifiedStamp = 0;
									
									if (modifiedProps.hasOwnProperty(prop)){
										modifiedStamp = modifiedProps[prop].timestamp;
									}
									if (parseFloat(modifiedStamp) >= parseFloat(qStamp))continue;
								}
								postData.properties.push({
									name	:prop,
									value	:qData[prop]
								});
							}
						}
						else postData = BSP.HSP_UTILS.getContactPostData(hubspotId, qData, action, qStamp);
					}
					var response = null;
					if (action == 'CREATE'){
						var recId = qRecs[i].getValue('custrecord_bsp_hsp_out_q_rec_id');
						if (recordType == 1)response = BSP.HSP_API.createACompany(postData);
						else response = BSP.HSP_API.createAContact(postData);
						if (response.getCode() < 299){
							var responseJSON = JSON.parse(response.getBody());
							
							if (recordType == 1){
								hubspotId = responseJSON.companyId.toString();
								nlapiSubmitField('customer', recId, 'custentity_bsp_hubspot_hubspot_id', hubspotId);
							}
							else {
								hubspotId = responseJSON.vid.toString();
								nlapiSubmitField('contact', recId, 'custentity_bsp_hubspot_hubspot_id', hubspotId);
							}
						}
					}
					else if (action == 'UPDATE'){
						if (!hubspotId){
							nlapiSubmitField('customrecord_bsp_hubspot_out_q', qId, ['custrecord_bsp_hsp_out_q_attempts', 'custrecord_bsp_hsp_out_q_err_message'], [parseInt(attempts) + 1, 'NO HUBSPOT ID']);
							continue;
						}
						if (recordType == 1)response = BSP.HSP_API.updateACompany(postData, hubspotId);
						else response = BSP.HSP_API.updateAContact(postData, hubspotId)
					}
					else if (action == 'DELETE'){
						if (!hubspotId){
							nlapiSubmitField('customrecord_bsp_hubspot_out_q', qId, ['custrecord_bsp_hsp_out_q_attempts', 'custrecord_bsp_hsp_out_q_err_message'], [parseInt(attempts) + 1, 'NO HUBSPOT ID']);
							continue;
						}
						if (recordType == 1)response = BSP.HSP_API.deleteACompany(hubspotId);
						else response = BSP.HSP_API.deleteAContact(hubspotId);
					}
					nlapiSubmitField('customrecord_bsp_hubspot_out_q', qId, 'custrecord_bsp_hsp_out_q_processed', 'T');
					var remainingUsage = nlapiGetContext().getRemainingUsage();
					if (response){
						if (response.getCode() > 299){
							if (!attempts)attempts = 0;
							nlapiSubmitField('customrecord_bsp_hubspot_out_q', qId, ['custrecord_bsp_hsp_out_q_attempts', 'custrecord_bsp_hsp_out_q_err_message'], [parseInt(attempts) + 1, response.getBody()]);
						}
					}
					if (remainingUsage < 150)nlapiYieldScript();
				}
				catch(e){
					BSP.HSP_ERROR_LOG.createErrorRecord(e);
				}
			}
		}
		catch(e){
			BSP.HSP_ERROR_LOG.createErrorRecord(e);
		}
	},
	__namespace : true
}
