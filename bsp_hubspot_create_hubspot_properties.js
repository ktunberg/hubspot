/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 Sep 2017     Karl Tunberg
 *
 */
if (typeof (BSP) == 'undefined') {
	BSP = {
		__namespace : true
	};
}
/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
BSP.HSP_PROPS = {
	createProperties : function(type){
		var existCtmrProps = BSP.HSP_UTILS.getMapFieldProps(1);
		var existContactProps = BSP.HSP_UTILS.getMapFieldProps(2);
		var contactResponse = BSP.HSP_API.getContactProperties();
		var ctmrResponse = BSP.HSP_API.getCompanyProperties();
		var contactProps = JSON.parse(contactResponse.getBody());
		var customerProps = JSON.parse(ctmrResponse.getBody());
		for (var i = 0; i < customerProps.length; i++){
			if (existCtmrProps.indexOf(customerProps[i].name) > -1)continue;
			BSP.HSP_UTILS.createFieldMapRec(customerProps[i].name, 1);
		}
		for (var j = 0; j < contactProps.length; j++){
			if (existContactProps.indexOf(contactProps[j].name) > -1)continue;
			BSP.HSP_UTILS.createFieldMapRec(contactProps[j].name, 2);
		}
	},
	__namespace : true
}