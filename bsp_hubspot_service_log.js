/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 Aug 2017     Karl Tunberg
 *
 */
if (typeof (BSP) == 'undefined') {
	BSP = {
		__namespace : true
	};
}

BSP.HSP_SERVICE_LOG = {
	createLog: function(url, method, postData, headers, response){
		var logRec = nlapiCreateRecord('customrecord_bsp_hubspot_service_log');
		logRec.setFieldValue('custrecord_bsp_hubspot_url', url);
		logRec.setFieldValue('custrecord_bsp_hubspot_method', method);
		logRec.setFieldValue('custrecord_bsp_hubspot_request_headers', JSON.stringify(headers));
		if (postData)logRec.setFieldValue('custrecord_bsp_hubspot_request_payload', JSON.stringify(postData));
		logRec.setFieldValue('custrecord_bsp_hubspot_response_code', response.getCode());
		logRec.setFieldValue('custrecord_bsp_hubspot_response_body', response.getBody());
		var logRecId = nlapiSubmitRecord(logRec, false, true);
		return logRecId;
	},
	__namespace : true
}
