/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Sep 2017     Karl Tunberg
 *
 */
if (typeof (BSP) == 'undefined') {
	BSP = {
		__namespace : true
	};
}
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
BSP.HSP_UE = {
	actionBefRecSubmit : function(type){
		var settings = BSP.HSP_SETTINGS.getActiveSettings();
		var execContext = nlapiGetContext().getExecutionContext();
		nlapiLogExecution('DEBUG', 'Execution Context', execContext);
		var fromHubspot = nlapiGetFieldValue('custentity_bsp_hubspot_from_hubspot');
		nlapiLogExecution('DEBUG', 'is Source Hubspot', fromHubspot);
		try{
			if (type == 'create')return;
			var recType = nlapiGetRecordType();
			var recId = nlapiGetRecordId(); 
			var origRec = nlapiLoadRecord(recType, recId);
			var hubspotId =origRec.getFieldValue('custentity_bsp_hubspot_hubspot_id');
			if (type == 'delete'){
				if (settings.deleteInHubspot == 'F')return;
				if (!hubspotId)return;
				var qId = BSP.HSP_UTILS.createOutQRec('DELETE', recType, hubspotId, {}, recId);
				return;
			}
			if (settings.updateInHubspot == 'F')return;
			var propObject = {};
			if (recType == 'customer') {
				var mapFields = BSP.HSP_UTILS.getMapFields(1, true, false);
			}
			else {
				var mapFields = BSP.HSP_UTILS.getMapFields(2, true, false);
			}
			if(!mapFields)return;
			var subrecFields = [];
			for (var i = 0; i < mapFields.length; i++){
				var subrec =  mapFields[i].getValue('custrecord_subrecord');
				var fieldName = mapFields[i].getValue('custrecord_bsp_hsp_netsuite_fld_id');
				var hubspotProp = mapFields[i].getValue('custrecord_bsp_hsp_property');
				var currentValue = nlapiGetFieldValue(fieldName);
				if (!hubspotId)return;
				var origValue = origRec.getFieldValue(fieldName);
				if (origValue == currentValue)continue;
				propObject[hubspotProp] = currentValue;
			}
			if (recType =='contact'){
				var currentCompany = nlapiGetFieldValue('company');
				var origCompany = origRec.getFieldValue('company');
				if (currentCompany &&(origCompany != currentCompany || !origCompany)){
					var companyId = nlapiLookupField('customer', currentCompany, 'custentity_bsp_hubspot_hubspot_id');
					if (companyId) propObject.associatedcompanyid = companyId;
				}
				
			}
			var qId = BSP.HSP_UTILS.createOutQRec('UPDATE', recType, hubspotId, propObject, recId);
			nlapiGetContext().setSetting('SESSION', 'qId', qId);
		}
		catch(e){
			BSP.HSP_ERROR_LOG.createErrorRecord(e);
		}
	},
	actionAftRecSubmit : function(type){
		var settings = BSP.HSP_SETTINGS.getActiveSettings();
		var execContext = nlapiGetContext().getExecutionContext();
		nlapiLogExecution('DEBUG', 'Execution Context', execContext);
		try{
			if (type != 'create'){
				var fromHubspot = nlapiGetFieldValue('custentity_bsp_hubspot_from_hubspot');
				if (fromHubspot != 'T'){
					return;
				}
				var qId = nlapiGetContext().getSetting('SESSION', 'qId');
				if (qId) nlapiDeleteRecord('customrecord_bsp_hubspot_out_q', qId);
				if (type!= 'delete'){
					nlapiSetFieldValue('custentity_bsp_hubspot_from_hubspot', 'F');
					nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'custentity_bsp_hubspot_from_hubspot', 'F');
				}
				return;
			}
			var recType = nlapiGetRecordType();
			var recId = nlapiGetRecordId(); 
			var fromHubspot = nlapiGetFieldValue('custentity_bsp_hubspot_from_hubspot');
			if (fromHubspot == 'T'){
				nlapiSubmitField(recType, recId, 'custentity_bsp_hubspot_from_hubspot', 'F');
				return;
			}
			nlapiSubmitField(recType, recId, 'custentity_bsp_hubspot_hubspot_id', null);
			if (settings.createInHubspot == 'F')return;
			var hubspotId = nlapiGetFieldValue('custentity_bsp_hubspot_hubspot_id');
			var propObject = {};
			if (recType == 'customer') var mapFields = BSP.HSP_UTILS.getMapFields(1, true, false);
			else {
				var mapFields = BSP.HSP_UTILS.getMapFields(2, true, false);
			}
			if(!mapFields)return;
			for (var i = 0; i < mapFields.length; i++){
				var fieldName = mapFields[i].getValue('custrecord_bsp_hsp_netsuite_fld_id');
				var currentValue = nlapiGetFieldValue(fieldName);
				if (!currentValue)continue;
				var hubspotProp = mapFields[i].getValue('custrecord_bsp_hsp_property');
				propObject[hubspotProp] = currentValue;
			}
			if (recType =='contact'){
				var currentCompany = nlapiGetFieldValue('company');
				if (currentCompany){
					var companyId = nlapiLookupField('customer', currentCompany, 'custentity_bsp_hubspot_hubspot_id');
					if (companyId) propObject.associatedcompanyid = companyId;
				}
				
			}
			BSP.HSP_UTILS.createOutQRec('CREATE', recType, hubspotId, propObject, recId);
		}
		catch(e){
			BSP.HSP_ERROR_LOG.createErrorRecord(e);
		}
	},
	__namespace : true
}
