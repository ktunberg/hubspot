/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 Aug 2017     Karl Tunberg
 *
 */
if (typeof (BSP) == 'undefined') {
	BSP = {
		__namespace : true
	};
}
/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */

BSP.HSP_IN = {
	createCustomerInboundQRecs : function(type){
		this.createInboundQs(1);
	},
	createContactInboundQRecs : function(type){
		this.createInboundQs(2);
	},
	createInboundQs : function(recType){
		try{
			var settings = BSP.HSP_SETTINGS.getActiveSettings();
			var response;
			var offset = null;
			var timeOffset = null;
			var paramstamp = settings.lastContInQ;
			if (recType == 1)paramstamp = settings.lastCtmrInQ;
			if(!paramstamp)paramstamp = 1;
			if (recType == 2) {
				response = BSP.HSP_API.getRecentlyModifiedContacts();
				var hasMore = JSON.parse(response.getBody())['has-more'];
				var hspRecs = JSON.parse(response.getBody()).contacts;
				var lastRecStamp = 0;
				if (hspRecs.length > 0){
					lastRecStamp = hspRecs[hspRecs.length - 1]['properties']['lastmodifieddate']['value'];
				}
				if(hasMore && parseFloat(lastRecStamp) > parseFloat(paramstamp)){
					offset = JSON.parse(response.getBody())['vid-offset'].toString();
					timeOffset = JSON.parse(response.getBody())['time-offset'].toString();
				}
			}
			else {
				response = BSP.HSP_API.getRecentlyModifiedCompanies();
				var hspRecs = JSON.parse(response.getBody()).results;
				var lastRecStamp = 0;
				if (hspRecs.length > 0){
					lastRecStamp = hspRecs[hspRecs.length - 1]['properties']['hs_lastmodifieddate']['value'];
				}
				var hasMore = JSON.parse(response.getBody()).hasMore;
				if (hasMore && parseFloat(lastRecStamp) > parseFloat(paramstamp)){
					offset = JSON.parse(response.getBody())['offset'].toString();
				}
			}
			var results = BSP.HSP_UTILS.addObjects(recType, offset, response, timeOffset, paramstamp);
			var mapRecResults = BSP.HSP_UTILS.getMapFields(recType, true, false);
			var mapArray = [];
			if (mapRecResults){
				for (var m = 0; m < mapRecResults.length; m++){
					mapArray.push(mapRecResults[m].getValue('custrecord_bsp_hsp_property'));
				}
			}
			var timestamp = paramstamp;	
			var lastModDate= 0;
			for (var i = 0; i < results.length; i++){
				if (recType == 1){
					lastModDate= results[i]['properties']['hs_lastmodifieddate']['value'];
				}
				else {
					lastModDate= results[i]['properties']['lastmodifieddate']['value'];
				}
				if (parseFloat(lastModDate) <= parseFloat(paramstamp))continue;
				var modifiedObj;
				if (recType == 2){
					modifiedObj = BSP.HSP_UTILS.processHubspotModifiedContacts(results, paramstamp, timestamp, settings, mapArray, i);
				}
				else {
					modifiedObj = BSP.HSP_UTILS.processHubspotModifiedCompanies(results, paramstamp, mapArray, i);
				}
				var objCount = Object.keys(modifiedObj.data.properties);
				if (objCount.length > 0) BSP.HSP_UTILS.createInboundQRec(recType, modifiedObj.data, modifiedObj.lastTimestamp);
				if (parseFloat(modifiedObj.lastTimestamp) > parseFloat(timestamp)) timestamp = modifiedObj.lastTimestamp;
			}
			if (parseFloat(timestamp) > parseFloat(paramstamp)){
				if (recType == 2){
					nlapiSubmitField('customrecord_bsp_hubspot_integr_settings', settings.recordId, 'custrecord_bsp_hubspot_last_cntct_in_q', timestamp);
				}
				else{
					nlapiSubmitField('customrecord_bsp_hubspot_integr_settings', settings.recordId, 'custrecord_bsp_hubspot_last_ctmr_in_q', timestamp);
				}
			}
		}
		catch(e){
			BSP.HSP_ERROR_LOG.createErrorRecord(e);
		}	
	},
	__namespace : true
}